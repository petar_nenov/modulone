const carBrands = [
    'Ford',
    'Renault',
    'Honda',
    'Dodge',
    'Cadillac',
    'Suzuki',
    'Kia',
    'Tata Motors',
    'Bentley',
    'Audi',
    'Fiat',
    'Land Rover',
];


for (const key in carBrands) {
    //console.log(key);
    if(key==='3') carBrands[key]=undefined;

}

console.log('-'.repeat(25));

for (const car of carBrands) {
    console.log(car);
}

console.log('-'.repeat(25));