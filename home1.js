// # Loops

// 1. Iterate over the properties of **obj** object

//    - Research which loop could iterate only the object (**firstName, lastName, age**) properties and write it

//      ```js
//      const humanObj = {
//        isHuman: true
//      };

//      // This will set the prototype of the object to be humanObj
//      // Do not think about this right now.

//      const obj = Object.create(humanObj);

//      obj.firstName = 'John';
//      obj.lastName = 'Doe';
//      obj.age = 28;
//      ```

//    - Expected output: (NOTE: isHuman is not in the result)

//      ```
//      John
//      Doe
//      28

const humanObj = {
  isHuman: true
};

const obj = Object.create(humanObj);

obj.firstName = "John";
obj.lastName = "Doe";
obj.age = 28;


for (const key in obj) {
    if(key!=="isHuman")  console.log(obj[key]);
}