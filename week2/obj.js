const _ = require("lodash");
const arr2 = [];

const func = () => {
  return "";
};

console.log(typeof arr2);
console.log(typeof func);

const person = {
  name: "Pesho",
  age: 47
}; //object literal

const objCopy = obj => {
  const newObj = {};
  for (const key in obj) {
    //console.log(key,obj[key])
    newObj[key] = obj[key];
  }

  return newObj;
};

const p2 = objCopy(person);

console.log(p2, person);

console.log(p2 === person);

const arr = [1, 2, 3, 4, 5];
arr.description = "simple numeric array";

console.table(arr);

let sum = 0;
for (const key in arr) {
  sum += key;
}
console.log(sum);

Object.defineProperty(person, "gender", {
  value: "male",
  writable: false,
  enumerable: true,
  configurble: false
});

console.log(person);

delete person.gender;

console.log(person);

//ES6

let city = "V.Tarnovo";

const p3 = {
  city
};

console.log(p3.city);

const p4 = {
  age: 27,
  adress: {
    street: "Yavorov"
  }
};

const p41 = JSON.stringify(p4); //to stringify
const p42 = JSON.parse(p41); //to object

console.log(typeof p42);

const p43 = _.cloneDeep(p4);

console.log(p43.adress === p4.adress);

const createShalowCopyFunctional = object => {
  const scp = Object.keys(object).reduce((copy, key) => {
    copy[key] = object[key];
    return copy;
  }, {});
  return scp;
};

const a = createShalowCopyFunctional(p4);
console.log(typeof a);
