const getGets = arr => {
  let index = 0;

  return () => {
    const toReturn = arr[index];
    index += 1;
    return toReturn;
  };
};
// this is the test
const test = [
  "6 6",
  "92 11 23 42 59 48",
  "09 92 23 72 56 14",
  "17 63 92 46 85 95",
  "34 12 52 69 23 95",
  "26 88 78 71 29 95",
  "26 34 16 63 39 95"
];

// const test = [
//   "6 6",
//   "92 11 11 42 59 48",
//   "09 92 23 72 56 14",
//   "12 63 92 46 85 95",
//   "34 12 52 52 52 95",
//   "12 88 78 71 29 95",
//   "0 0 0 0 0 0"
// ];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const matrix = [];
let [row, col] = gets()
  .split(" ")
  .map(Number);

//console.log(row, col);

for (let r = 0; r < row; r++) {
  matrix[r] = gets()
    .split(" ")
    .map(Number);
}

//console.table(matrix);
let max = 0;

const rigth = (r, c) => {
  let len = 1;
  let current = matrix[r][c];


  while (c < col - 1) {
    if (current === matrix[r][c + 1]) len++;
    else {
      break;
    }
    c++;
  }
  return len;
};

const downRight = (r, c) => {
  let len = 1;
  let current = matrix[r][c];


  while (c < col - 1 && r < row - 1) {
    if (current === matrix[r + 1][c + 1]) len++;
    else {
      break;
    }
    c++;
    r++;
  }
  return len;
};

const down = (r, c) => {
  let len = 1;
  let current = matrix[r][c];


  while (r < row - 1) {
    if (current === matrix[r + 1][c]) len++;
    else {
      break;
    }
    r++;
  }
  return len;
};

const downLeft = (r, c) => {
  let len = 1;
  let current = matrix[r][c];


  while (c > 0 && r < row - 1) {
    if (current === matrix[r + 1][c - 1]) len++;
    else {
      break;
    }
    c--;
    r++;
  }
  return len;
};


for (let r = 0; r < row; r++) {
  for (let c = 0; c < col; c++) {
    let maxRight = rigth(r, c);

    //console.log(right);
    let maxDownRight = downRight(r, c);
    //console.log(downright);
    let maxDown = down(r, c);
    //console.log(down);
    let maxDownLeft = downLeft(r, c);
    //console.log(downleft);    
    max = Math.max(max, maxRight, maxDownRight, maxDown, maxDownLeft);
  }
}

print(max);
