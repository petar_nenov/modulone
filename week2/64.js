/**
 * @param {number[][]} grid
 * @return {number}
 */
var minPathSum = function(arr) {
  for (let col = 1; col < arr[0].length; col++) {
    arr[0][col] = arr[0][col] + arr[0][col - 1];
  }
  for (let row = 1; row < arr.length; row++) {
    arr[row][0] = arr[row][0] + arr[row - 1][0];
  }

  for (let row = 1; row < arr.length; row++) {
    for (let col = 1; col < arr[0].length; col++) {
      //console.log(row,col);
      arr[row][col] =
        Math.min(arr[row - 1][col], arr[row][col - 1]) + arr[row][col];
    }
  }

  const ans = arr[arr.length - 1][arr[0].length - 1];

  return ans;
};
