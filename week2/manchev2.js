const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
  '4',
  '10 291 19 206'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;


const n = gets();
const arr = gets().split(' ');

const maxNumber = (arr) => {
    const max = [];
    // eslint-disable-next-line guard-for-in
    for (const index in arr) {
        for (const char of arr[index]) {
            max.push(char);
        }
    }
    return max;
};

const numbersToString = maxNumber(arr);

//console.log(numbersToString);


for (let i = 0; i < numbersToString.length; i ++) {
    if (numbersToString[i] != numbersToString[numbersToString.length - 1-i]) {
        numbersToString.shift();
        numbersToString.pop();
    }
}

//console.log(numbersToString)

while(!(+numbersToString[0])){
    numbersToString.shift();
    numbersToString.pop();
}


print(numbersToString.join(''));