const sizeOfMatrix = gets().split(' ').map(Number);
const row = sizeOfMatrix[0];
const col = sizeOfMatrix[1];
const matrix = [];


for (let i = 0; i < row; i++) {
    const current = gets().split(' ');
    matrix[i] = [];
    for (let j = 0; j < col; j++) {
        matrix[i].push(current[j]);
    }
}
 //console.table(matrix);
// console.table(matrix);
// check line.
let lineResult = 1;
for (let i = 0; i < row ; i++) {
    let line = 1;
    for (let j = 0; j <= col - 1; j++) {
        if (matrix[i][j] === matrix[i][j + 1]) {
            line++;
            if (line > lineResult) {
                lineResult = line;
            }
        } else {
            line = 1;
        }
    }
}
console.log(lineResult)
// check column.
let columnResult = 1;
for (let i = 0; i < col; i++) {
    let column = 1;
    for (let j = 0; j < row - 1; j++) {
        if (matrix[j][i] === matrix[j + 1][i]) {
            column++;
            if (column > columnResult) {
                columnResult = column;
            }
        } else {
            column = 1;
        }
    }
}
console.log(columnResult);


let diagonalResult1 = 1;
//left rigth
for (let k = 1; k < row ; k ++) {
    let diagonal = 1;
    let i = k;
    let j = 0;
    while (i > 0 && j < col-1) {
        if (matrix[i][j] === matrix[i - 1][j + 1]) {
            diagonal ++;
            if (diagonal > diagonalResult1) {
                diagonalResult1 = diagonal;
            }
        } else {
            diagonal = 1;
        }
        i = i - 1;
        j = j + 1;
    }
}

for (let k = 1; k < col - 1; k ++) {
    i = row - 1;
    j = k;
    let diagonal1 = 1;
    while (j < col-1 && i > 0)  {
        if (matrix[i][j] === matrix[i - 1][j + 1]) {
            diagonal1 ++;
            if (diagonal1 > diagonalResult1) {
                diagonalResult1 = diagonal1;
            }
        } else {
            diagonal1 = 1;
        }
        i -= 1;
        j =j + 1;
    }
}

// check diagonal from right to left.
let diagonalResult2 = 1;
for (let k = 1; k <row ; k ++) {
    let diagonal2 = 1;
    let i = k;
    let j = col - 1;
    while (i > 0 && j>0) {
        if (matrix[i][j] === matrix[i - 1][j - 1]) {
            diagonal2++;
            if (diagonal2 > diagonalResult2) {
                diagonalResult2 = diagonal2;
            }
        } else {
            diagonal2 = 1;
        }
        i -= 1;
        j -= 1;
    }
}

for (let k = 1; k < col; k ++) {
    let diagonal2 = 1;
    let i = row - 1;
    let j = col - 1 - k;
    while (j > 0 && i > 0) {
        if (matrix[i][j] === matrix[i - 1][j - 1]) {
            diagonal2++;
            if (diagonal2 > diagonalResult2) {
                diagonalResult2 = diagonal2;
            } else {
                diagonal2 = 1;
            }
        }
        i -=1;
        j -=1;
    }
}

print(Math.max(lineResult, columnResult, diagonalResult1, diagonalResult2));
