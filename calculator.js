const add = (a, b) => a + b;
const sub = (a, b) => a - b;
const mul = (a, b) => a * b;
const div = (a, b) => a / b;

const calculator = (a, b, ...funcs) => {
  const result = [];
  for (const func of funcs) {
    result.push(func(a, b));
  }
  return result;
};

console.log(calculator(2, 3, sub, add, mul, div));
