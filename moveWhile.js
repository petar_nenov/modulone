/* eslint-disable no-console */
const getGets = arr => {
  let index = 0;

  return () => {
    const toReturn = arr[index];
    index += 1;
    return toReturn;
  };
};
// this is the test
//const test = ["talk", "hablar"];
//const test = ['thank you',
//'muchas gracias'];

//const test = ["test", "el examen"];

//const test = ['abvgfdssdas','abvgd'];

const test = [
  "0",
  "10,20,30,40,50",
  "2 forward 1",
  "2 backwards 1",
  "3 forward 2",
  "3 backwards 2",
  "exit"
];

// const test = [
//   "4",
//   "10,20,30,40,50",
//   "2 forward 1",
//   "2 backwards 1",
//   "3 forward 2",
//   "3 backwards 2",
//   "exit"
// ];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const pos = +gets();

const data = gets()
  .split(",")
  .map(Number);

//print(post, data);

let command = gets();

let currentPosition = pos;

//   const forward = (c1,c3)=>{
//       let sum = 0;
//       for(let jump=0;jump<c1;jump++){
//           let tempPos = (currentPosition + c3) % data.length;
//           currentPosition = tempPos;
//           //print('temp forwar',tempPos);
//           sum +=data[currentPosition];
//       }
//       return sum;
//   }

const forward = (step, size) => {
  let sum = 0;
  for (let jump = 0; jump < step; jump++) {
    let tempPosition = currentPosition + size;
    while (tempPosition > data.length - 1) {
      tempPosition -= data.length;
    }
    sum += data[tempPosition];
    currentPosition = tempPosition;
  }
  return sum;
};

// const backward = (c1, c3) => {
//   let sum = 0;
//   for (let jump = 0; jump < c1; jump++) {
//     let tempPos = (currentPosition - c3) % data.length;
//     //print('temp position backword',tempPos);
//     if (tempPos < 0) tempPos = data.length + tempPos;
//     //print('temp position backword',tempPos);
//     currentPosition = tempPos;
//     //print('temp forwar',tempPos);
//     sum += data[currentPosition];
//   }
//   return sum;
// };

const backward = (step, size) => {
  let sum = 0;
  for (let jump = 0; jump < step; jump++) {
    let tempPosition = currentPosition - size;
    while (tempPosition < 0) {
      tempPosition += data.length;
    }
    sum += data[tempPosition];
    currentPosition = tempPosition;
  }
  return sum;
};

let sumF = 0;
let sumB = 0;

while (command !== "exit") {
  const tempCmd = command.split(" ");
  const step = +tempCmd[0];
  const cmd = tempCmd[1];
  const size = +tempCmd[2];
  //console.log(c1, c2, c3);

  if (cmd === "forward") {
    let sf = forward(step, size);
    //print(currentPosition,sf)
    sumF += sf;
  } else {
    let sb = backward(step, size);
    //print(currentPosition,sb)
    sumB += sb;
  }

  command = gets();
}

// Forward: 150
// Backwards: 120

print(`Forward: ${sumF}`);
print(`Backwards: ${sumB}`);
