const getGets = arr => {
  let index = 0;

  return () => {
    const toReturn = arr[index];
    index += 1;
    return toReturn;
  };
};
// this is the test
//const test = ["talk", "hablar"];
const test = ['thank you',
'muchas gracias'];

//const test = ["test", "el examen"];

//const test = ['abvgfdssdas','abvgd'];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

let w1 = gets();
let w2 = gets();

//console.log(w1, w2);

let result = "";

while (w1.length && w2.length) {
  let c1 = w1.substr(0, 1);
  //console.log(c1, c1.charCodeAt());
  w1 = w1.substr(1, w1.length);
  let c2 = w2.substr(0, 1);
  
  //console.log(c2, c2.charCodeAt());

  w2 = w2.substr(1, w2.length);

  let tempC;
  let newC;



  if (c1 === " " || c1 === "-") {
    tempC = c1;
    //console.log('c1 is empty')
    //console.log(c1);
    newC = tempC;
  } else if (c2 === " " || c2 === "-") {
    tempC = c2;
    //console.log('c2 is empty')
    //console.log(c2);
    newC = tempC;
  } else {
    tempC = Math.abs(c1.charCodeAt() - c2.charCodeAt());
    newC = String.fromCharCode(tempC + 97);
  }

  //console.log(tempC);

  result += newC;
  //console.log(newC);

  //console.log('a'.charCodeAt());
}

if (w1.length < w2.length) {
  
  let toAdd = w2.substr(w1.length);
  //console.log(toAdd);
  result += toAdd;
}

if (w1.length > w2.length) {
  
    let toAdd = w1.substr(w2.length);
    //console.log(toAdd);
    result += toAdd;
  }

print(result);
