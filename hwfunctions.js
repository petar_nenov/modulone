// # Functions

// 1. Implement the following:
//     - **"forEach"** function that accepts an array and a function with one parameter and executes the passed function with each of the items in the array as a parameter

//     - **"filter"** function that accepts an array and a function with one parameter that returns boolean value based on the function logic **(predicate function)**, executes the passed function with each of the items in the array as a parameter and returns new array with only the items that verified the condition of the passed function
//         ```
//         array: [ 42, 15, 18, 23, 6 ]
//         predicate: ((item) => (item % 2) === 0)
//         result: [ 42, 18, 6 ]
//         ```

//     - **"some"** function that accepts an array and a function with one parameter that returns boolean value based on the function logic **(predicate function)**, executes the passed function with each of the items in the array as a parameter and returns *true* if at least one of the items verified the condition of the passed function
//         ```
//         array: [ 42, 15, 18, 23, 6 ]
//         predicate: ((item) => item === 42)
//         result: true
//         ```

//     - **"every"** function that accepts an array and a function with one parameter that returns boolean value based on the function logic **(predicate function)**, executes the passed function with each of the items in the array as a parameter and returns *true* if at all of the items verified the condition of the passed function
//         ```
//         array: [ 42, 15, 18, 23, 6 ]
//         predicate: ((item) => item > 5)
//         result: true
//         ```

  
//     - **"findIndex"** function that accepts an array and a function with one parameter that returns boolean value based on the function logic **(predicate function)**, executes the passed function with each of the items in the array as a parameter and returns the index of the first item that verified the condition of the passed function. If no item verified the condition, return **-1**
//         ```
//         array: [ 42, 15, 18, 23, 6 ]
//         predicate: ((item) => item === 18)
//         result: 2
//         ```

//     - **"map"** function that accepts an array and a function with one parameter that returns a value based on the function logic, executes the passed function with each of the items in the array as a parameter and returns new array that holds all of the results of the passed function invocations
//         ```
//         array: [ 42, 15, 18, 23, 6 ]
//         function: ((item) => item * 2)
//         result: [ 84, 30, 36, 46, 12 ]
//         ```

//     - **"reduce"** function that accepts: 1 - an array, 2 - a function with two parameters - the first will be current aggregated value and the second will be the current item of the array, 3 - a starting aggregating value. The passed function should collect something from the current item, store it in the aggregated value and return the updated aggregated value. At the end the *reduce* function should have collected something from all of the items in the array and return the fully aggregated value from all of them.
//         ```
//         array: [ 42, 15, 18, 23, 6 ]
//         function: ((sum, currentValue) => {
//             sum += currentValue;
//             return sum;
//         })
//         starting aggregating value: 0
//         result: 104
//         ```

const myForEach = (arr,callback)=>{
    const result = [];
    for (const e of arr) {
        result.push(callback(e));
    }
    return result;
};

const arr = [1,2,3,4];

const newArr = myForEach(arr,(e)=>e/2);

console.table(newArr);



