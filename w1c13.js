const getGets = arr => {
  let index = 0;

  return () => {
    const toReturn = arr[index];
    index += 1;
    return toReturn;
  };
};

const test = [
  "2 2",
  "0 1",
  "1 1",
  "1 0",
  "1 1",
  "P1 1 1",
  "P2 0 1",
  "P1 0 0",
  "P2 1 1",
  "P1 1 1",
  "P2 1 0",
  "END"
];
const gets = this.gets || getGets(test);
const print = this.print || console.log;

const line1 = gets()
  .split(" ")
  .map(Number);

const row = line1[0];
const col = line1[1];

//print(row, col);

const board1 = [];
const board2 = [];

let allP1=0;
let allP2=0;

const conutOne = (arr)=>{
    let sum =0;

    for (const row of arr) {
        for (const cell of row) {
            if(cell===1) sum++;
        }
    }

    return sum;
}

for (let r = 0; r < row; r++) {
  board1[r] = gets()
    .split(" ")
    .map(Number); 

}

for (let r = 0; r < row; r++) {
  board2.unshift(
    gets()
      .split(" ")
      .map(Number)
  );
  
}

allP1=conutOne(board1);
allP2=conutOne(board2);

//console.table(board1);

//console.table(board2);

let rP1=0;
let rP2=0;

const goP1 = (r, c) => {
  let shut = "Missed";
  if (board2[r][c]===1) {
    shut = "Booom";  
    rP1++;  
  }
  else if(board2[r][c]==='M')shut = 'Try again!'; 
  board2[r][c] = 'M'; 
  return shut;
};

const goP2 = (r, c) => {
    let shut = "Missed";
    if (board1[r][c]===1) {
      shut = "Booom";  
      rP2++;    
    }
    else if(board1[r][c]==='M')shut = 'Try again!';  
    board1[r][c] = 'M';
    return shut;
  };


let command = gets();

while (command !== "END") {
  let tempCmd = command.split(" ");
  let p = tempCmd[0];
  let r = tempCmd[1];
  let c = tempCmd[2];

  //print(p, r, c);

  if (p === "P1") {
    let move=goP1(r, c);
    print(move);
  } else {
    let move=goP2(r, c);
    print(move);
  }

//   console.table(board1);
//   console.table(board2);


  command = gets();
}


let result=`${allP1-rP2}:${allP2-rP1}`;

print(result);