const pesho = {
  name: "Pesho",
  age: 25,
  sex: "male"
};

const ivan = {
  name: "Ivan",
  age: 22,
  sex: "male"
};

const petq = {
  name: "Petq",
  age: 27,
  sex: "female"
};

const mariq = {
  name: "Mariq",
  age: 23,
  sex: "female"
};

const georgi = {
  name: "Georgi",
  age: 29,
  sex: "male"
};

const people = [pesho, ivan, petq, mariq, georgi];

const showName = peoples => {
  for (const people of peoples) {
    console.log(people.name);
  }
};

showName(people);

const showFixAge = (peoples, age) => {
  for (const people of peoples) {
    if (people.age <= age) console.log(people.name);
  }
};

showFixAge(people, 31);

// console.log(pesho.name);
// console.log(ivan.name);
// console.log(petq.name);
// console.log(mariq.name);
// console.log(georgi.name);

// if (pesho.age < 30 && ivan.age < 30 && petq.age < 30 && mariq.age < 30 && georgi.age < 30) {
//     console.log('All under 30!');
// }

// const sumAge = pesho.age + ivan.age + petq.age + mariq.age + georgi.age;
// console.log(`The sum of the age of all people is ${sumAge}`);

const sumAllPeople = peoples => {
  let sum = 0;
  for (const people of peoples) {
    sum += people.age;
  }
  return sum;
};

console.log(sumAllPeople(people));

// if (pesho.sex === "male") {
//   males.push(pesho);
// }
// if (ivan.sex === "male") {
//   males.push(ivan);
// }
// if (petq.sex === "male") {
//   males.push(petq);
// }
// if (mariq.sex === "male") {
//   males.push(mariq);
// }
// if (georgi.sex === "male") {
//   males.push(georgi);
// }

const females = [];
const males = [];

const sortSex = peoples => {
  for (const people of peoples) {
    if (people.sex === "male") {
      males.push(people.name);
    } else {
      females.push(people.name);
    }
  }
};

sortSex(people);

console.log("males", males);

// if (pesho.sex === "female") {
//   females.push(pesho);
// }
// if (ivan.sex === "female") {
//   females.push(ivan);
// }
// if (petq.sex === "female") {
//   females.push(petq);
// }
// if (mariq.sex === "female") {
//   females.push(mariq);
// }
// if (georgi.sex === "female") {
//   females.push(georgi);
// }

console.log("females", females);
